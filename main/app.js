// function distance(first, second) {
//   //   const first = ['a'];
//   //   const second = ['a', 'b'];
// }

// const first = ['a'];
// const second = ['a', 'b'];

// let unique =
//   second.filter(y => !first.includes(y)) +
//   first.filter(y => !second.includes(y));
// console.log(unique);

function distance(first, second) {
  // let first = ['a'];
  // let second = ['a', 'b'];

  let arr =
    second.filter(y => !first.includes(y)) +
    first.filter(y => !second.includes(y));

  if (first instanceof Array && second instanceof Array) {
    console.log('Congrats, your parameters are bot arrays');
  } else {
    throw new Error(
      'Invalid Type - make sure that your both parameters are arrays'
    );
  }

  return arr;
}

module.exports.distance = distance;
